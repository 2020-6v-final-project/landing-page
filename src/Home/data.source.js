import React from 'react';
export const Nav30DataSource = {
  wrapper: { className: 'header3 home-page-wrapper kq5bazf1krd-editor_css' },
  page: { className: 'home-page' },
  logo: {
    className: 'header3-logo kq5b9uqsq4o-editor_css',
    children: 'https://i.imgur.com/RPQDgCq.png',
  },
  Menu: {
    className: 'header3-menu kq5bagn82ro-editor_css',
    children: [
      {
        name: 'item3',
        className: 'header3-item',
        children: {
          href: '#',
          children: [{ children: '导航四', name: 'text' }],
        },
      },
    ],
  },
  mobileMenu: { className: 'header3-mobile-menu' },
};
export const Banner50DataSource = {
  wrapper: { className: 'home-page-wrapper banner5 kq4yndtpbz-editor_css' },
  page: { className: 'home-page banner5-page kq4wbkew6jf-editor_css' },
  childWrapper: {
    className: 'banner5-title-wrapper',
    children: [
      {
        name: 'title',
        children: (
          <span>
            <p>Cook.it</p>
          </span>
        ),
        className: 'banner5-title kq14boyirek-editor_css',
      },
      {
        name: 'title~kq14cd5uhap',
        className: 'kq4ykk7kmn-editor_css',
        children: (
          <span>
            <span>
              <span>
                <p>Đơn giản, hiệu quả, tiết kiệm</p>
              </span>
            </span>
          </span>
        ),
      },
      {
        name: 'content',
        className: 'banner5-content kq4ykgthyp6-editor_css',
        children: (
          <span>
            <span>
              <span>
                <span>
                  <span>
                    <span>
                      <p>
                        Giải pháp tự động hóa quy trình Phục vụ - Order - Thu
                        ngân - Báo cáo, đảm bảo tối ưu nguồn lực, giảm thiểu sai
                        sót, tiết kiệm chi phí.
                      </p>
                    </span>
                  </span>
                </span>
              </span>
            </span>
          </span>
        ),
      },
    ],
  },
  image: {
    className: 'banner5-image kq14926lhia-editor_css',
    children:
      'https://image.freepik.com/free-vector/headwaiter-welcoming-customer-cafe-woman-sitting-down-table-waiter-accepting-order-flat-vector-illustration-restaurant-service_74855-13135.jpg',
  },
};
export const Content00DataSource = {
  wrapper: { className: 'home-page-wrapper content0-wrapper' },
  page: { className: 'home-page content0 kq4uzv1q7pt-editor_css' },
  OverPack: { playScale: 0.3, className: '' },
  titleWrapper: {
    className: 'title-wrapper',
    children: [
      {
        name: 'title',
        children: (
          <span>
            <span>
              <span>
                <span>
                  <p>Vì sao chọn Cook.it?</p>
                </span>
              </span>
            </span>
          </span>
        ),
      },
    ],
  },
  childWrapper: {
    className: 'content0-block-wrapper',
    children: [
      {
        name: 'block0',
        className: 'content0-block',
        md: 8,
        xs: 24,
        children: {
          className: 'content0-block-item',
          children: [
            {
              name: 'image',
              className: 'content0-block-icon kq58il1dku7-editor_css',
              children:
                'https://image.flaticon.com/icons/png/512/1110/1110801.png',
            },
            {
              name: 'title',
              className: 'content0-block-title kq58ghs0zk6-editor_css',
              children: (
                <span>
                  <span>
                    <span>
                      <span>
                        <span>
                          <p>Tiết kiệm</p>
                        </span>
                      </span>
                    </span>
                  </span>
                </span>
              ),
            },
            {
              name: 'content',
              children: (
                <span>
                  <p>
                    <b>Cook.it</b> giúp tự động hoá hầu hết các thao tác quản
                    lý, nghiệp vụ, giúp tiết kiệm tối đa chi phí nhân lực.
                  </p>
                </span>
              ),
              className: 'kq4xxmpu9v-editor_css',
            },
          ],
        },
      },
      {
        name: 'block1',
        className: 'content0-block',
        md: 8,
        xs: 24,
        children: {
          className: 'content0-block-item',
          children: [
            {
              name: 'image',
              className: 'content0-block-icon kq58ik33k9-editor_css',
              children:
                'https://image.flaticon.com/icons/png/512/1110/1110822.png',
            },
            {
              name: 'title',
              className: 'content0-block-title kq58grhnbwf-editor_css',
              children: (
                <span>
                  <span>
                    <span>
                      <p>Thân thiện</p>
                    </span>
                  </span>
                </span>
              ),
            },
            {
              name: 'content',
              children: (
                <span>
                  <span>
                    <p>
                      Với giao diện đơn giản, trực quan và thân thiện, người
                      dùng chỉ phải mất khoảng <b>30 phút</b> để làm quen với{' '}
                      <b>Cook.it</b>.
                    </p>
                  </span>
                </span>
              ),
              className: 'kq4xy926rag-editor_css',
            },
          ],
        },
      },
      {
        name: 'block2',
        className: 'content0-block',
        md: 8,
        xs: 24,
        children: {
          className: 'content0-block-item',
          children: [
            {
              name: 'image',
              className: 'content0-block-icon kq58s7c82b-editor_css',
              children:
                'https://image.flaticon.com/icons/png/512/1110/1110818.png',
            },
            {
              name: 'title',
              className: 'content0-block-title kq58guph8ca-editor_css',
              children: (
                <span>
                  <span>
                    <span>
                      <p>Đa dạng</p>
                    </span>
                  </span>
                </span>
              ),
            },
            {
              name: 'content',
              children: (
                <span>
                  <span>
                    <p>
                      <b>Cook.it</b> cung cấp các tính năng cho các lĩnh vực{' '}
                      <b>nhà hàng, quán ăn, trà sữa</b>, đảm bảo đầy đủ các thao
                      tác nghiệp vụ.
                    </p>
                  </span>
                </span>
              ),
              className: 'kq4y368wft-editor_css',
            },
          ],
        },
      },
      {
        name: 'block~kq4v1ddqnw',
        className: 'content0-block kq4y5tf666-editor_css',
        md: 8,
        xs: 24,
        children: {
          className: 'content0-block-item',
          children: [
            {
              name: 'image',
              className: 'content0-block-icon kq58u3tpyk-editor_css',
              children:
                'https://image.flaticon.com/icons/png/512/1110/1110792.png',
            },
            {
              name: 'title',
              className: 'content0-block-title kq58gyikfdq-editor_css',
              children: (
                <span>
                  <span>
                    <span>
                      <p>Tiện lợi</p>
                    </span>
                  </span>
                </span>
              ),
            },
            {
              name: 'content',
              children: (
                <span>
                  <span>
                    <span>
                      <p>
                        Với <b>Cook.it</b>
                        <span>
                          , bạn không cần phải trải qua những bước cài đặt phức
                          tạp. Tất cả những gì bạn cần chỉ là một máy tính được
                          kết nối internet.
                        </span>
                      </p>
                    </span>
                  </span>
                </span>
              ),
              className: 'kq4y44q69od-editor_css',
            },
          ],
        },
      },
      {
        name: 'block~kq4v1gj9bv',
        className: 'content0-block kq4y613iqx-editor_css',
        md: 8,
        xs: 24,
        children: {
          className: 'content0-block-item',
          children: [
            {
              name: 'image',
              className: 'content0-block-icon kq58vuuystt-editor_css',
              children:
                'https://image.flaticon.com/icons/png/512/1967/1967117.png',
            },
            {
              name: 'title',
              className: 'content0-block-title kq58h2sju6j-editor_css',
              children: (
                <span>
                  <span>
                    <span>
                      <p>&nbsp;Chuyên nghiệp</p>
                    </span>
                  </span>
                </span>
              ),
            },
            {
              name: 'content',
              children: (
                <span>
                  <span>
                    <p>
                      <b>Cook.it</b> cung cấp các tính năng cho các lĩnh vực{' '}
                      <b>nhà hàng, quán ăn, trà sữa</b>, đảm bảo đầy đủ các thao
                      tác nghiệp vụ.
                    </p>
                  </span>
                </span>
              ),
              className: 'kq4y4bz8q8-editor_css',
            },
          ],
        },
      },
      {
        name: 'block~kq4v1hst1hh',
        className: 'content0-block kq4y64zuctn-editor_css',
        md: 8,
        xs: 24,
        children: {
          className: 'content0-block-item',
          children: [
            {
              name: 'image',
              className: 'content0-block-icon kq58wlfrna6-editor_css',
              children:
                'https://image.flaticon.com/icons/png/512/1967/1967103.png',
            },
            {
              name: 'title',
              className: 'content0-block-title kq58h60m0da-editor_css',
              children: (
                <span>
                  <span>
                    <span>
                      <p>Toàn diện</p>
                    </span>
                  </span>
                </span>
              ),
            },
            {
              name: 'content',
              children: (
                <span>
                  <span>
                    <span>
                      <span>
                        <p>
                          Toàn bộ nghiệp vụ của nhà hàng từ{' '}
                          <b>xếp bàn, gọi món, chế biến</b> đến thanh toán đều
                          diễn ra trên một phần mềm duy nhất.<br />
                        </p>
                      </span>
                    </span>
                  </span>
                </span>
              ),
              className: 'kq4y4hfvat6-editor_css',
            },
          ],
        },
      },
    ],
  },
};
export const Feature80DataSource = {
  wrapper: {
    className: 'home-page-wrapper feature8-wrapper kq4vx6xrkct-editor_css',
  },
  page: { className: 'home-page feature8' },
  OverPack: { playScale: 0.3 },
  titleWrapper: {
    className: 'feature8-title-wrapper',
    children: [
      {
        name: 'title',
        className: 'feature8-title-h1',
        children: (
          <span>
            <p>Quy trình Phục vụ - Thu ngân - Bếp được tự động hóa</p>
          </span>
        ),
      },
      {
        name: 'content',
        className: 'feature8-title-content kq4vqybj1mj-editor_css',
        children: (
          <span>
            <span>
              <p>
                Bởi <b>Cook.it</b> - Phần mềm quản lý quán ăn, nhà hàng chuyên
                nghiệp
              </p>
            </span>
          </span>
        ),
      },
    ],
  },
  childWrapper: {
    className: 'feature8-button-wrapper',
    children: [
      {
        name: 'button',
        className: 'feature8-button kq59cwqmlul-editor_css',
        children: { href: '#', children: '立即体验' },
      },
    ],
  },
  Carousel: {
    dots: false,
    className: 'feature8-carousel',
    wrapper: { className: 'feature8-block-wrapper' },
    children: {
      className: 'feature8-block',
      titleWrapper: {
        className: 'feature8-carousel-title-wrapper',
        title: { className: 'feature8-carousel-title' },
      },
      children: [
        {
          name: 'block0',
          className: 'feature8-block-row kq4yapi3yv-editor_css',
          gutter: 120,
          title: {
            className: 'feature8-carousel-title-block',
            children: '平台自主训练流程',
          },
          children: [
            {
              className: 'feature8-block-col',
              md: 6,
              xs: 24,
              name: 'child0',
              arrow: {
                className: 'feature8-block-arrow',
                children:
                  'https://gw.alipayobjects.com/zos/basement_prod/167bee48-fbc0-436a-ba9e-c116b4044293.svg',
              },
              children: {
                className: 'feature8-block-child',
                children: [
                  {
                    name: 'image',
                    className: 'feature8-block-image',
                    children:
                      'https://www.sapo.vn/Themes/Portal/Default/StylesV2/images/fnb/index/icon-step-1.png',
                  },
                  {
                    name: 'title',
                    className: 'feature8-block-title kq4ydx0mlks-editor_css',
                    children: (
                      <span>
                        <p>
                          Tạo yêu cầu <b>gọi món</b> của khách và áp dụng{' '}
                          <b>khuyến mãi</b>
                        </p>
                      </span>
                    ),
                  },
                ],
              },
            },
            {
              className: 'feature8-block-col',
              md: 6,
              xs: 24,
              name: 'child1',
              arrow: {
                className: 'feature8-block-arrow',
                children:
                  'https://gw.alipayobjects.com/zos/basement_prod/167bee48-fbc0-436a-ba9e-c116b4044293.svg',
              },
              children: {
                className: 'feature8-block-child',
                children: [
                  {
                    name: 'image',
                    className: 'feature8-block-image',
                    children:
                      'https://www.sapo.vn/Themes/Portal/Default/StylesV2/images/fnb/index/icon-step-2.png',
                  },
                  {
                    name: 'title',
                    className: 'feature8-block-title kq4ye4a3ti6-editor_css',
                    children: (
                      <span>
                        <span>
                          <p>
                            Chuyển phiếu gọi món đến thẳng <b>quầy bar/ bếp</b>{' '}
                            và chế biến
                          </p>
                        </span>
                      </span>
                    ),
                  },
                ],
              },
            },
            {
              className: 'feature8-block-col',
              md: 6,
              xs: 24,
              name: 'child2',
              arrow: {
                className: 'feature8-block-arrow',
                children:
                  'https://gw.alipayobjects.com/zos/basement_prod/167bee48-fbc0-436a-ba9e-c116b4044293.svg',
              },
              children: {
                className: 'feature8-block-child',
                children: [
                  {
                    name: 'image',
                    className: 'feature8-block-image',
                    children:
                      'https://www.sapo.vn/Themes/Portal/Default/StylesV2/images/fnb/index/icon-step-3.png',
                  },
                  {
                    name: 'title',
                    className: 'feature8-block-title kq4yfvcyafl-editor_css',
                    children: (
                      <span>
                        <span>
                          <span>
                            <p>
                              Yêu cầu <b>thanh toán</b>, thu ngân nhận yêu cầu
                              và <b>tính tiền</b>
                            </p>
                          </span>
                        </span>
                      </span>
                    ),
                  },
                ],
              },
            },
            {
              className: 'feature8-block-col',
              md: 6,
              xs: 24,
              name: 'child3',
              arrow: {
                className: 'feature8-block-arrow',
                children:
                  'https://gw.alipayobjects.com/zos/basement_prod/167bee48-fbc0-436a-ba9e-c116b4044293.svg',
              },
              children: {
                className: 'feature8-block-child',
                children: [
                  {
                    name: 'image',
                    className: 'feature8-block-image',
                    children:
                      'https://www.sapo.vn/Themes/Portal/Default/StylesV2/images/fnb/index/icon-step-4.png',
                  },
                  {
                    name: 'title',
                    className: 'feature8-block-title kq4yg8vyho-editor_css',
                    children: (
                      <span>
                        <span>
                          <p>
                            Quản lý xem <b>báo cáo</b> kinh doanh mọi lúc mọi
                            nơi
                          </p>
                        </span>
                      </span>
                    ),
                  },
                ],
              },
            },
          ],
        },
      ],
    },
  },
};
export const Feature60DataSource = {
  wrapper: { className: 'home-page-wrapper feature6-wrapper' },
  OverPack: { className: 'home-page feature6', playScale: 0.3 },
  Carousel: {
    className: 'feature6-content',
    dots: false,
    wrapper: { className: 'feature6-content-wrapper' },
    titleWrapper: {
      className: 'feature6-title-wrapper',
      barWrapper: {
        className: 'feature6-title-bar-wrapper',
        children: { className: 'feature6-title-bar kq561k6bsg7-editor_css' },
      },
      title: { className: 'feature6-title' },
    },
    children: [
      {
        title: {
          className: 'feature6-title-text kq5615p2vhp-editor_css',
          children: (
            <span>
              <p>Tổng quan về Cook.it</p>
            </span>
          ),
        },
        className: 'feature6-item',
        name: 'block0',
        children: [
          {
            md: 6,
            xs: 12,
            className: 'feature6-number-wrapper kq56yg5ck3m-editor_css',
            name: 'child0',
            number: {
              className: 'feature6-number kq561zzafwp-editor_css',
              unit: {
                className: 'feature6-unit kq5625o4trd-editor_css',
                children: '万',
              },
              toText: true,
              children: '41',
            },
            children: {
              className: 'feature6-text kq56v58zmv-editor_css',
              children: (
                <span>
                  <p>Chức năng vận hành</p>
                </span>
              ),
            },
          },
          {
            md: 6,
            xs: 12,
            className: 'feature6-number-wrapper kq56ydaabuu-editor_css',
            name: 'child~kq564abslj',
            number: {
              className: 'feature6-number kq561zzafwp-editor_css',
              unit: {
                className: 'feature6-unit kq5625o4trd-editor_css',
                children: '万',
              },
              toText: true,
              children: '10',
            },
            children: {
              className: 'feature6-text kq56vbukan6-editor_css',
              children: (
                <span>
                  <span>
                    <p>Loại khuyến mãi hỗ trợ</p>
                  </span>
                </span>
              ),
            },
          },
          {
            md: 6,
            xs: 12,
            className: 'feature6-number-wrapper kq56y5cb0o-editor_css',
            name: 'child~kq564b9aq0k',
            number: {
              className: 'feature6-number kq561zzafwp-editor_css',
              unit: {
                className: 'feature6-unit kq5625o4trd-editor_css',
                children: '万',
              },
              toText: true,
              children: '10',
            },
            children: {
              className: 'feature6-text kq56vfpai3h-editor_css',
              children: (
                <span>
                  <span>
                    <p>&nbsp;Loại báo cáo hỗ trợ</p>
                  </span>
                </span>
              ),
            },
          },
          {
            md: 6,
            xs: 12,
            className: 'feature6-number-wrapper kq56yitepd-editor_css',
            name: 'child~kq564c67o8n',
            number: {
              className: 'feature6-number kq561zzafwp-editor_css',
              unit: {
                className: 'feature6-unit kq5625o4trd-editor_css',
                children: '万',
              },
              toText: true,
              children: '3',
            },
            children: {
              className: 'feature6-text kq56vjli14t-editor_css',
              children: (
                <span>
                  <span>
                    <p>Loại hình kinh doanh</p>
                  </span>
                </span>
              ),
            },
          },
        ],
      },
    ],
  },
};
export const Content10DataSource = {
  wrapper: { className: 'home-page-wrapper content1-wrapper' },
  OverPack: { className: 'home-page content1', playScale: 0.3 },
  imgWrapper: { className: 'content1-img', md: 10, xs: 24 },
  img: {
    children:
      'https://www.sapo.vn/Themes/Portal/Default/StylesV2/images/fnb/index/img-function-1.png',
    className: 'kq4wj8ikpxq-editor_css',
  },
  textWrapper: {
    className: 'content1-text kq4vd5gizge-editor_css',
    md: 14,
    xs: 24,
  },
  title: {
    className: 'content1-title',
    children: (
      <span>
        <span>
          <p>Đơn giản hoá việc quản lý</p>
        </span>
      </span>
    ),
  },
  content: {
    className: 'content1-content kq4ytt2robi-editor_css',
    children: (
      <span>
        <p>
          Với <b>Cook.it</b>,<b>&nbsp;</b>bạn không còn cần phải bận tâm đến
          việc quản lý sổ sách, giấy tờ hay các thủ tục phức tạp khác. Tất cả dữ
          liệu đều sẽ được hệ thống <b>thu thập</b>, <b>tính toán</b> và{' '}
          <b>thống kê</b> một cách đầy đủ và chính xác.
        </p>
      </span>
    ),
  },
};
export const Feature20DataSource = {
  wrapper: { className: 'home-page-wrapper content2-wrapper' },
  OverPack: { className: 'home-page content2', playScale: 0.3 },
  imgWrapper: { className: 'content2-img', md: 10, xs: 24 },
  img: {
    children:
      'https://www.sapo.vn/Themes/Portal/Default/StylesV2/images/fnb/index/img-function-2.png',
    className: 'kq4wi8h48m-editor_css',
  },
  textWrapper: {
    className: 'content2-text kq4wjdn8udt-editor_css',
    md: 14,
    xs: 24,
  },
  title: {
    className: 'content2-title',
    children: (
      <span>
        <span>
          <span>
            <span>
              <span>
                <p>Tự động hoá các nghiệp vụ</p>
              </span>
            </span>
          </span>
        </span>
      </span>
    ),
  },
  content: {
    className: 'content2-content kq4yvdbrave-editor_css',
    children: (
      <span>
        <p>
          Từ các thao tác <b>chọn món, áp dụng khuyến mãi, thanh toán</b> đến{' '}
          <b>tương tác với đầu bếp, cập nhật trạng thái món</b>,... Tất cả đều
          được <b>tự động hoá</b>, giúp tiết kiệm tối đa nhân công, nâng cao
          trải nghiệm khách hàng.
        </p>
      </span>
    ),
  },
};
export const Feature10DataSource = {
  wrapper: { className: 'home-page-wrapper content1-wrapper' },
  OverPack: { className: 'home-page content1', playScale: 0.3 },
  imgWrapper: { className: 'content1-img', md: 10, xs: 24 },
  img: {
    children:
      'https://www.sapo.vn/Themes/Portal/Default/StylesV2/images/fnb/index/img-function-3.png',
    className: 'kq4wjz2xmbd-editor_css',
  },
  textWrapper: {
    className: 'content1-text kq4wif5u0y-editor_css',
    md: 14,
    xs: 24,
  },
  title: {
    className: 'content1-title',
    children: (
      <span>
        <p>Kiểm soát chặt chẽ</p>
      </span>
    ),
  },
  content: {
    className: 'content1-content kq4yvkn874a-editor_css',
    children: (
      <span>
        <p>
          Các <b>số liệu, đơn hàng, ca làm, doanh thu</b>... đều được hệ thống{' '}
          <b>ghi nhận</b>, <b>tính toán</b> và <b>đảm bảo</b>, hạn chế tối đa
          việc thất thoát, gian lận.&nbsp;
        </p>
      </span>
    ),
  },
};
export const Content90DataSource = {
  wrapper: { className: 'home-page-wrapper content9-wrapper' },
  page: { className: 'home-page content9' },
  titleWrapper: {
    className: 'title-wrapper',
    children: [
      {
        name: 'image',
        children:
          'https://gw.alipayobjects.com/zos/rmsportal/PiqyziYmvbgAudYfhuBr.svg',
        className: 'title-image',
      },
      {
        name: 'title',
        children: (
          <span>
            <span>
              <p>&nbsp;Tính năng nổi bật</p>
            </span>
          </span>
        ),
        className: 'title-h1',
      },
    ],
  },
  block: {
    className: 'timeline',
    children: [
      {
        name: 'block1',
        className: 'block-wrapper',
        playScale: 0.3,
        children: {
          imgWrapper: { className: 'image-wrapper kq4urhn253j-editor_css' },
          content: {
            className: 'block-content kq4z6fyfl4-editor_css',
            children: (
              <span>
                <span>
                  <span>
                    <span>
                      <span>
                        <p>
                          <b>Giới hạn</b> khả năng truy cập vào các chức năng hệ
                          thống của từng nhân viên, đảm bảo mỗi nhân viên chỉ có
                          quyền truy cập vào nhóm chức năng{' '}
                          <b>ứng với vai trò của mình</b>.
                        </p>
                      </span>
                    </span>
                  </span>
                </span>
              </span>
            ),
          },
          name: {
            className: 'block-name kq4zbxd52kp-editor_css',
            children: (
              <span>
                <span>
                  <span>
                    <p>Quyền hạn</p>
                  </span>
                </span>
              </span>
            ),
          },
          textWrapper: { className: 'text-wrapper' },
          time: {
            className: 'block-time kq4z65s2unm-editor_css',
            children: (
              <span>
                <p>Mô tả</p>
              </span>
            ),
          },
          icon: {
            className: 'block-icon',
            children:
              'https://gw.alipayobjects.com/zos/rmsportal/QviGtUPvTFxdhsTUAacr.svg',
          },
          title: {
            className: 'block-title kq4z6bvgbc6-editor_css',
            children: (
              <span>
                <span>
                  <span>
                    <span>
                      <span>
                        <p>Quyền hạn của nhân viên</p>
                      </span>
                    </span>
                  </span>
                </span>
              </span>
            ),
          },
          post: {
            className: 'block-post kq4zckc9x9m-editor_css',
            children: (
              <span>
                <span>
                  <p>
                    <br />
                  </p>
                </span>
              </span>
            ),
          },
          img: {
            className: 'block-img kq4zbca20xj-editor_css',
            children:
              'https://image.flaticon.com/icons/png/512/1330/1330279.png',
          },
        },
      },
      {
        name: 'block2',
        className: 'block-wrapper',
        playScale: 0.3,
        children: {
          imgWrapper: { className: 'image-wrapper kq59pfrlbyb-editor_css' },
          content: {
            className: 'block-content kq4z6xjxyj-editor_css',
            children: (
              <span>
                <span>
                  <p>
                    Cho phép tạo các chương trình <b>khuyến mãi</b> với nhiều
                    hình thức:<b>tặng món, giảm giá</b>,... với các điều
                    kiện áp dụng đa dạng:{' '}
                    <b>giá đơn tối thiểu, combo, món khuyến mãi</b>,... giúp chủ
                    nhà hàng/quán ăn có thể tạo ra khuyến mãi phù hợp với nhu
                    cầu.
                  </p>
                </span>
              </span>
            ),
          },
          name: {
            className: 'block-name kq4zf7a7ot8-editor_css',
            children: (
              <span>
                <p>Quản lý khuyến mãi</p>
              </span>
            ),
          },
          textWrapper: { className: 'text-wrapper' },
          time: {
            className: 'block-time kq4z6qd3o-editor_css',
            children: (
              <span>
                <p>Mô tả</p>
              </span>
            ),
          },
          icon: {
            className: 'block-icon',
            children:
              'https://gw.alipayobjects.com/zos/rmsportal/QviGtUPvTFxdhsTUAacr.svg',
          },
          title: {
            className: 'block-title kq4z6u4sxu-editor_css',
            children: (
              <span>
                <span>
                  <span>
                    <p>Chương trình khuyến mãi</p>
                  </span>
                </span>
              </span>
            ),
          },
          post: {
            className: 'block-post kq4zcj1s9ms-editor_css',
            children: (
              <span>
                <span>
                  <p>
                    <br />
                  </p>
                </span>
              </span>
            ),
          },
          img: {
            className: 'block-img kq4zbg4ll5-editor_css',
            children:
              'https://image.flaticon.com/icons/png/512/4088/4088812.png',
          },
        },
      },
      {
        name: 'block3',
        className: 'block-wrapper',
        playScale: 0.3,
        children: {
          imgWrapper: { className: 'image-wrapper kq59uxfpwy7-editor_css' },
          content: {
            className: 'block-content kq4z7jo3qcl-editor_css',
            children: (
              <span>
                <span>
                  <p>
                    Hỗ trợ <b>theo dõi</b>, <b>giám sát</b> quá trình nhập/xuất
                    hàng hoá. Cho phép thống kê, tính toán, quản lý nguồn cung
                    cấp hàng hoá,...
                  </p>
                </span>
              </span>
            ),
          },
          name: {
            className: 'block-name kq4zqxna64-editor_css',
            children: (
              <span>
                <p>Quản lý kho hàng</p>
              </span>
            ),
          },
          textWrapper: { className: 'text-wrapper' },
          time: {
            className: 'block-time kq4z7xgfjzu-editor_css',
            children: (
              <span>
                <p>Mô tả</p>
              </span>
            ),
          },
          icon: {
            className: 'block-icon',
            children:
              'https://gw.alipayobjects.com/zos/rmsportal/agOOBdKEIJlQhfeYhHJc.svg',
          },
          title: {
            className: 'block-title kq4z7sztoqp-editor_css',
            children: (
              <span>
                <span>
                  <p>Tình trạng hàng hoá</p>
                </span>
              </span>
            ),
          },
          post: {
            className: 'block-post kq4ztq4d8to-editor_css',
            children: (
              <span>
                <p>\b</p>
              </span>
            ),
          },
          img: {
            className: 'block-img kq4zbl71pgn-editor_css',
            children: 'https://image.flaticon.com/icons/png/512/869/869078.png',
          },
        },
      },
    ],
  },
};
export const Teams30DataSource = {
  wrapper: { className: 'home-page-wrapper teams3-wrapper' },
  page: { className: 'home-page teams3' },
  OverPack: { playScale: 0.3, className: '' },
  titleWrapper: {
    className: 'title-wrapper',
    children: [
      {
        name: 'title',
        children: (
          <span>
            <span>
              <span>
                <p>Giải pháp quản lý chuyên nghiệp</p>
              </span>
            </span>
          </span>
        ),
        className: 'kq508cypea6-editor_css',
      },
    ],
  },
  blockTop: {
    className: 'block-top-wrapper',
    children: [
      {
        name: 'block0',
        className: 'block-top kq50ao3esfl-editor_css',
        md: 8,
        xs: 24,
        titleWrapper: {
          children: [
            {
              name: 'image',
              className: 'teams3-top-image kq5a17qrfyl-editor_css',
              children:
                'https://image.flaticon.com/icons/png/512/2405/2405283.png',
            },
            {
              name: 'title',
              className: 'teams3-top-title',
              children: (
                <span>
                  <span>
                    <p>Nhân viên</p>
                  </span>
                </span>
              ),
            },
            {
              name: 'content1',
              className: 'teams3-top-content kq570jh7md9-editor_css',
              children: (
                <span>
                  <span>
                    <span>
                      <span>
                        <span>
                          <p>
                            Quản lý hoạt động phát sinh có liên quan đến nhân
                            viên, ghi nhận lại toàn bộ{' '}
                            <b>
                              lịch sử bán hàng, chi tiết doanh số, doanh thu
                            </b>{' '}
                            bán hàng của nhân viên.
                          </p>
                        </span>
                      </span>
                    </span>
                  </span>
                </span>
              ),
            },
          ],
        },
      },
      {
        name: 'block1',
        className: 'block-top',
        md: 8,
        xs: 24,
        titleWrapper: {
          children: [
            {
              name: 'image',
              className: 'teams3-top-image kq5a2ykc6b6-editor_css',
              children:
                'https://image.flaticon.com/icons/png/512/602/602176.png',
            },
            {
              name: 'title',
              className: 'teams3-top-title',
              children: (
                <span>
                  <span>
                    <p>Chi nhánh</p>
                  </span>
                </span>
              ),
            },
            {
              name: 'content1',
              className: 'teams3-top-content kq573idedva-editor_css',
              children: (
                <span>
                  <span>
                    <span>
                      <span>
                        <span>
                          <span>
                            <p>
                              Chủ quán có thể theo dõi nhân viên, ca làm, tình
                              hình kinh doanh, quản lý kho, tài chính{' '}
                              <b>theo từng chi nhánh</b>.<br />
                            </p>
                          </span>
                        </span>
                      </span>
                    </span>
                  </span>
                </span>
              ),
            },
          ],
        },
      },
      {
        name: 'block2',
        className: 'block-top kq50b9s2qn-editor_css',
        md: 8,
        xs: 24,
        titleWrapper: {
          children: [
            {
              name: 'image',
              className: 'teams3-top-image kq5a3jcs3r-editor_css',
              children:
                'https://image.flaticon.com/icons/png/512/639/639364.png',
            },
            {
              name: 'title',
              className: 'teams3-top-title',
              children: (
                <span>
                  <p>Tài chính</p>
                </span>
              ),
            },
            {
              name: 'content1',
              className: 'teams3-top-content kq577q0254s-editor_css',
              children: (
                <span>
                  <p>
                    Theo dõi tình hình <b>thu/chi, doanh thu</b> ngày, thống kê,
                    xuất báo cáo <b>doanh thu</b> theo từng mốc thời gian khác
                    nhau.
                  </p>
                </span>
              ),
            },
          ],
        },
      },
    ],
  },
  block: {
    className: 'block-wrapper',
    gutter: 72,
    children: [
      {
        name: 'block0',
        className: 'block',
        md: 8,
        xs: 24,
        image: {
          name: 'image',
          className: 'teams3-image kq5adxqkmbd-editor_css',
          children: 'https://image.flaticon.com/icons/png/512/4363/4363168.png',
        },
        titleWrapper: {
          className: 'teams3-textWrapper',
          children: [
            {
              name: 'title',
              className: 'teams3-title kq579gw0u6j-editor_css',
              children: (
                <span>
                  <span>
                    <span>
                      <span>
                        <span>
                          <p>Món</p>
                        </span>
                      </span>
                    </span>
                  </span>
                </span>
              ),
            },
            {
              name: 'content1',
              className: 'teams3-content kq57fhxiqu-editor_css',
              children: (
                <span>
                  <p>Thêm, xoá, sửa thông tin liên quan đến món ăn.</p>
                </span>
              ),
            },
          ],
        },
      },
      {
        name: 'block~kq57imnjkt9',
        className: 'block',
        md: 8,
        xs: 24,
        image: {
          name: 'image',
          className: 'teams3-image kq5agqk4j1c-editor_css',
          children: 'https://image.flaticon.com/icons/png/512/4363/4363142.png',
        },
        titleWrapper: {
          className: 'teams3-textWrapper',
          children: [
            {
              name: 'title',
              className: 'teams3-title kq579gw0u6j-editor_css',
              children: (
                <span>
                  <span>
                    <span>
                      <p>Nguyên liệu</p>
                    </span>
                  </span>
                </span>
              ),
            },
            {
              name: 'content1',
              className: 'teams3-content kq57fhxiqu-editor_css',
              children: (
                <span>
                  <span>
                    <span>
                      <span>
                        <p>
                          Thêm, xoá, sửa thông tin liên quan đến nguyên vật
                          liệu.
                        </p>
                      </span>
                    </span>
                  </span>
                </span>
              ),
            },
          ],
        },
      },
      {
        name: 'block~kq57incp8w7',
        className: 'block',
        md: 8,
        xs: 24,
        image: {
          name: 'image',
          className: 'teams3-image kq5ahpks99i-editor_css',
          children: 'https://image.flaticon.com/icons/png/512/4363/4363245.png',
        },
        titleWrapper: {
          className: 'teams3-textWrapper',
          children: [
            {
              name: 'title',
              className: 'teams3-title kq579gw0u6j-editor_css',
              children: (
                <span>
                  <span>
                    <span>
                      <span>
                        <span>
                          <p>Thực đơn</p>
                        </span>
                      </span>
                    </span>
                  </span>
                </span>
              ),
            },
            {
              name: 'content1',
              className: 'teams3-content kq57fhxiqu-editor_css',
              children: (
                <span>
                  <span>
                    <span>
                      <p>Quản lý các thông tin về thực đơn.</p>
                    </span>
                  </span>
                </span>
              ),
            },
          ],
        },
      },
      {
        name: 'block~kq57iolj66g',
        className: 'block',
        md: 8,
        xs: 24,
        image: {
          name: 'image',
          className: 'teams3-image kq5ai0gkcwt-editor_css',
          children: 'https://image.flaticon.com/icons/png/512/4363/4363256.png',
        },
        titleWrapper: {
          className: 'teams3-textWrapper',
          children: [
            {
              name: 'title',
              className: 'teams3-title kq579gw0u6j-editor_css',
              children: (
                <span>
                  <span>
                    <span>
                      <span>
                        <span>
                          <span>
                            <p>Đơn hàng</p>
                          </span>
                        </span>
                      </span>
                    </span>
                  </span>
                </span>
              ),
            },
            {
              name: 'content1',
              className: 'teams3-content kq57fhxiqu-editor_css',
              children: (
                <span>
                  <span>
                    <span>
                      <p>Quản lý các thông tin về đơn hàng.</p>
                    </span>
                  </span>
                </span>
              ),
            },
          ],
        },
      },
      {
        name: 'block~kq57ip8r8x',
        className: 'block',
        md: 8,
        xs: 24,
        image: {
          name: 'image',
          className: 'teams3-image kq5aogc0mib-editor_css',
          children: 'https://image.flaticon.com/icons/png/512/1967/1967047.png',
        },
        titleWrapper: {
          className: 'teams3-textWrapper',
          children: [
            {
              name: 'title',
              className: 'teams3-title kq579gw0u6j-editor_css',
              children: (
                <span>
                  <span>
                    <span>
                      <span>
                        <span>
                          <span>
                            <p>Ca làm</p>
                          </span>
                        </span>
                      </span>
                    </span>
                  </span>
                </span>
              ),
            },
            {
              name: 'content1',
              className: 'teams3-content kq57fhxiqu-editor_css',
              children: (
                <span>
                  <span>
                    <span>
                      <span>
                        <p>Quản lý các thông tin về ca llàm của nhân viên.</p>
                      </span>
                    </span>
                  </span>
                </span>
              ),
            },
          ],
        },
      },
      {
        name: 'block~kq57iq29jfl',
        className: 'block',
        md: 8,
        xs: 24,
        image: {
          name: 'image',
          className: 'teams3-image kq5aonm8h8-editor_css',
          children: 'https://image.flaticon.com/icons/png/512/4363/4363205.png',
        },
        titleWrapper: {
          className: 'teams3-textWrapper',
          children: [
            {
              name: 'title',
              className: 'teams3-title kq579gw0u6j-editor_css',
              children: (
                <span>
                  <span>
                    <span>
                      <span>
                        <p>Các chứng năng khác</p>
                      </span>
                    </span>
                  </span>
                </span>
              ),
            },
            {
              name: 'content1',
              className: 'teams3-content kq57fhxiqu-editor_css',
              children: (
                <span>
                  <span>
                    <span>
                      <span>
                        <span>
                          <p>
                            Hỗ trợ tối đa nghiệp vụ quản lý và hoạt động của
                            quán.
                          </p>
                        </span>
                      </span>
                    </span>
                  </span>
                </span>
              ),
            },
          ],
        },
      },
    ],
  },
};
export const Content40DataSource = {
  wrapper: {
    className: 'home-page-wrapper content4-wrapper kq5bizq2dm-editor_css',
  },
  page: { className: 'home-page content4' },
  OverPack: { playScale: 0.3, className: '' },
  titleWrapper: {
    className: 'title-wrapper',
    children: [
      {
        name: 'title',
        children: (
          <span>
            <span>
              <p>Cook.it là mọi thứ bạn cần</p>
            </span>
          </span>
        ),
        className: 'title-h1',
      },
      {
        name: 'content',
        className:
          'title-content content4-title-content kq5be2j5hto-editor_css',
        children: (
          <span>
            <span>
              <span>
                <p>
                  {' '}
                  để quản lý nhà hàng, quán ăn một cách hiệu quả và tiết kiệm
                </p>
              </span>
            </span>
          </span>
        ),
      },
    ],
  },
  video: {
    className: 'content4-video',
    children: {
      video: 'https://os.alipayobjects.com/rmsportal/EejaUGsyExkXyXr.mp4',
      image: 'https://zos.alipayobjects.com/rmsportal/HZgzhugQZkqUwBVeNyfz.jpg',
    },
  },
};
export const Content110DataSource = {
  OverPack: {
    className: 'home-page-wrapper content11-wrapper',
    playScale: 0.3,
    appear: true,
  },
  titleWrapper: {
    className: 'title-wrapper',
    children: [
      {
        name: 'image',
        children:
          'https://gw.alipayobjects.com/zos/rmsportal/PiqyziYmvbgAudYfhuBr.svg',
        className: 'title-image',
      },
      {
        name: 'title',
        children: (
          <span>
            <p>Liên hệ với chúng tôi</p>
          </span>
        ),
        className: 'title-h1',
      },
      {
        name: 'content',
        children: (
          <span>
            <p>
              Để nhận những giải đáp thắc mắc, cũng như những thông tin chi tiết
              về sản phẩm
            </p>
          </span>
        ),
        className: 'title-content kq5bmvi6oe-editor_css',
      },
      {
        name: 'content2',
        children: '现场问诊，为你答疑解难',
        className: 'title-content kq5bnphh6g9-editor_css',
      },
    ],
  },
  button: {
    className: '',
    children: {
      a: {
        className: 'button kq5bnt7buhb-editor_css',
        href: 'mailto:hello@gmail.com',
        children: (
          <span>
            <span>
              <p>Liên hệ</p>
            </span>
          </span>
        ),
        type: 'primary',
      },
    },
  },
};
